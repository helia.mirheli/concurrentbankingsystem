package com.example.concurrentbankingsystem.database;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ACCOUNT")
@Entity
public class BankAccountEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_PK", nullable = false)
    private Long id;

    @Column(name = "ACCOUNT_NAME", length = 50, nullable = false)
    private String accountName;

    @Column(name = "ACCOUNT_NO", length = 50, nullable = false, unique = true)
    private String accountNumber;

    @Column(name = "BALANCE", nullable = false)
    private double balance;
}
