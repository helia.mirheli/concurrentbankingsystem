package com.example.concurrentbankingsystem.bl.Bank.repository.service.command;

import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountOut;
import com.example.concurrentbankingsystem.bl.Bank.repository.dao.BankAccountDao;
import com.example.concurrentbankingsystem.logger.TransactionObserver;

public interface TransactionCommand {
    BankAccountOut execute(BankAccountDao bankAccountDao, TransactionObserver observer);
}
