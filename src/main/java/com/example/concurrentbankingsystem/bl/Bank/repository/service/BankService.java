package com.example.concurrentbankingsystem.bl.Bank.repository.service;

import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountIn;
import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountOut;
import com.example.concurrentbankingsystem.bl.Bank.repository.dao.BankAccountDao;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.command.TransactionCommand;
import com.example.concurrentbankingsystem.database.BankAccountEntity;
import com.example.concurrentbankingsystem.logger.TransactionObserver;
import jakarta.annotation.PreDestroy;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class BankService {
    private final BankAccountDao bankAccountDao;
    private final ModelMapper modelMapper;
    private final ExecutorService executorService;
    private final TransactionObserver observer;

    @Autowired
    public BankService(BankAccountDao bankAccountDao, ModelMapper modelMapper, TransactionObserver observer) {
        this.bankAccountDao = bankAccountDao;
        this.modelMapper = modelMapper;
        this.observer = observer;
        this.executorService = Executors.newFixedThreadPool(3);
    }

    public BankAccountOut createAccount(BankAccountIn model) {
        BankAccountEntity newEntity = modelMapper.map(model, BankAccountEntity.class);
        newEntity = bankAccountDao.save(newEntity);
        return new BankAccountOut(newEntity);
    }


    public Future<BankAccountOut> performTransaction(TransactionCommand command) {
        return executorService.submit(() -> command.execute(bankAccountDao, observer));
    }

    public double getBalance(String accountNumber) {
        BankAccountEntity accountEntity = bankAccountDao.findByAccountNumber(accountNumber);
        return accountEntity != null ? accountEntity.getBalance() : 0;
    }

    @PreDestroy
    public void cleanup() {
        executorService.shutdown();
    }
}
