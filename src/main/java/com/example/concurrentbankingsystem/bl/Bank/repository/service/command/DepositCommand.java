package com.example.concurrentbankingsystem.bl.Bank.repository.service.command;

import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountOut;
import com.example.concurrentbankingsystem.bl.Bank.repository.dao.BankAccountDao;
import com.example.concurrentbankingsystem.database.BankAccountEntity;
import com.example.concurrentbankingsystem.logger.TransactionObserver;

public class DepositCommand implements TransactionCommand {
    private final String accountNumber;
    private final double amount;

    public DepositCommand(String accountNumber, double amount) {
        this.accountNumber = accountNumber;
        this.amount = amount;
    }

    @Override
    public BankAccountOut execute(BankAccountDao bankAccountDao, TransactionObserver observer) {
        BankAccountEntity accountEntity = bankAccountDao.findByAccountNumber(accountNumber);

        if (accountEntity != null) {
            synchronized (accountEntity) {
                accountEntity.setBalance(accountEntity.getBalance() + amount);
                accountEntity = bankAccountDao.save(accountEntity);
            }

            observer.onTransaction(accountNumber, "deposit", amount);
            return new BankAccountOut(accountEntity);
        }
        return null;
    }
}
