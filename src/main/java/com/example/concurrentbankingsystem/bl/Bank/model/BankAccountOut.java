package com.example.concurrentbankingsystem.bl.Bank.model;

import com.example.concurrentbankingsystem.database.BankAccountEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BankAccountOut extends BankAccountIn {
    private long id;

    public BankAccountOut(BankAccountEntity entity) {
        super(entity);
        if (entity != null) {
            this.id = entity.getId();
        }
    }
}
