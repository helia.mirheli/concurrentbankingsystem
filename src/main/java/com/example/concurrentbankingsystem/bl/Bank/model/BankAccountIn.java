package com.example.concurrentbankingsystem.bl.Bank.model;

import com.example.concurrentbankingsystem.database.BankAccountEntity;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BankAccountIn {

    @NotNull
    @NotEmpty
    private String accountNumber;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 30)
    private String accountName;

    @NotNull
    @Min(10)
    private double balance;

    public BankAccountIn(BankAccountEntity entity) {
        if (entity != null) {
            this.accountNumber = entity.getAccountNumber();
            this.accountName = entity.getAccountName();
            this.balance = entity.getBalance();
        }
    }


    @Override
    public String toString() {
        return "{" +
                "Account Number: " + accountNumber +
                ", Account Name: " + accountName +
                ", Balance: " + balance +
                "}";
    }
}
