package com.example.concurrentbankingsystem.bl.Bank.repository.service.command;

import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountOut;
import com.example.concurrentbankingsystem.bl.Bank.repository.dao.BankAccountDao;
import com.example.concurrentbankingsystem.database.BankAccountEntity;
import com.example.concurrentbankingsystem.logger.TransactionObserver;

public class TransferCommand implements TransactionCommand {
    private final String fromAccountNumber;
    private final String toAccountNumber;
    private final double amount;

    public TransferCommand(String fromAccountNumber, String toAccountNumber, double amount) {
        this.fromAccountNumber = fromAccountNumber;
        this.toAccountNumber = toAccountNumber;
        this.amount = amount;
    }

    @Override
    public BankAccountOut execute(BankAccountDao bankAccountDao, TransactionObserver observer) {
        BankAccountEntity fromAccountEntity = bankAccountDao.findByAccountNumber(fromAccountNumber);
        BankAccountEntity toAccountEntity = bankAccountDao.findByAccountNumber(toAccountNumber);

        if (fromAccountEntity != null && toAccountEntity != null && fromAccountEntity.getBalance() >= amount) {
            synchronized (fromAccountEntity) {
                synchronized (toAccountEntity) {
                    fromAccountEntity.setBalance(fromAccountEntity.getBalance() - amount);
                    toAccountEntity.setBalance(toAccountEntity.getBalance() + amount);

                    fromAccountEntity = bankAccountDao.save(fromAccountEntity);
                    toAccountEntity = bankAccountDao.save(toAccountEntity);
                }
            }

            observer.onTransaction(fromAccountNumber, "transfer", amount);
            return new BankAccountOut(fromAccountEntity);
        }
        return null;
    }
}
