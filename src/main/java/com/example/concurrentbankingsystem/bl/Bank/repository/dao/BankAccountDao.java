package com.example.concurrentbankingsystem.bl.Bank.repository.dao;

import com.example.concurrentbankingsystem.database.BankAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccountDao extends JpaRepository<BankAccountEntity, Long> {
    BankAccountEntity findByAccountNumber(String accountNumber);
}
