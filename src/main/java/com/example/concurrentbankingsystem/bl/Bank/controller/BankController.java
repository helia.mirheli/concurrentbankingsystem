package com.example.concurrentbankingsystem.bl.Bank.controller;

import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountIn;
import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountOut;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.BankService;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.command.DepositCommand;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.command.TransferCommand;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.command.WithdrawCommand;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.Future;

@RestController
@RequestMapping("/api/bank")
public class BankController {
    private final BankService bankService;

    @Autowired
    public BankController(BankService bankService) {
        this.bankService = bankService;
    }

    @PostMapping("/accounts")
    public ResponseEntity<BankAccountOut> createAccount(@RequestBody @Valid BankAccountIn model) {
        BankAccountOut accountOut = bankService.createAccount(model);
        return ResponseEntity.status(accountOut != null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR).body(accountOut);
    }

    @PostMapping("/deposit")
    public ResponseEntity<Future<BankAccountOut>> deposit(@RequestParam("accountNumber") String accountNumber,
                                                          @RequestParam("amount") double amount) {
        Future<BankAccountOut> accountOut = bankService.performTransaction(new DepositCommand(accountNumber, amount));
        return ResponseEntity.status(accountOut != null ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(accountOut);
    }

    @PostMapping("/withdraw")
    public ResponseEntity<Future<BankAccountOut>> withdraw(@RequestParam("accountNumber") String accountNumber,
                                                           @RequestParam(value = "amount") double amount) {
        Future<BankAccountOut> accountOut = bankService.performTransaction(new WithdrawCommand(accountNumber, amount));
        return ResponseEntity.status(accountOut != null ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(accountOut);
    }

    @PostMapping("/transfer")
    public ResponseEntity<Future<BankAccountOut>> transfer(@RequestParam("fromAccountNumber") String fromAccountNumber,
                                                           @RequestParam("toAccountNumber") String toAccountNumber,
                                                           @RequestParam("amount") double amount) {
        Future<BankAccountOut> accountOut = bankService.performTransaction(new TransferCommand(fromAccountNumber, toAccountNumber, amount));
        return ResponseEntity.status(accountOut != null ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(accountOut);
    }

    @GetMapping("/balance")
    public ResponseEntity<Double> getBalance(@RequestParam("accountNumber") String accountNumber) {
        double balance = bankService.getBalance(accountNumber);
        return balance != 0 ? ResponseEntity.ok(balance) : ResponseEntity.notFound().build();
    }
}
