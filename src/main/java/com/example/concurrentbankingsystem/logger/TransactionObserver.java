package com.example.concurrentbankingsystem.logger;

public interface TransactionObserver {
    void onTransaction(String accountNumber, String transactionType, double amount);
}
