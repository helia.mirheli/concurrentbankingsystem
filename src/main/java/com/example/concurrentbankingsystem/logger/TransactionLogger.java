package com.example.concurrentbankingsystem.logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class TransactionLogger implements TransactionObserver {
    private final String fileName;
    private final List<String> transactions;

    public TransactionLogger(@Value("${logging.file.name}") String fileName) {
        this.fileName = fileName;
        this.transactions = new ArrayList<>();
    }

    @Override
    public void onTransaction(String accountNumber, String transactionType, double amount) {
        String transactionMessage = buildTransactionMessage(accountNumber, transactionType, amount);
        transactions.add(transactionMessage);
        logTransactionToFile(transactionMessage);
    }

    private String buildTransactionMessage(String accountNumber, String transactionType, double amount) {
        LocalDateTime timestamp = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedTimestamp = timestamp.format(formatter);
        return "[" + formattedTimestamp + "] " + "Account: " + accountNumber + " | Transaction Type: " +
                transactionType + " | Amount: " + amount;
    }

    private void logTransactionToFile(String transactionMessage) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(fileName, true))) {
            writer.println(transactionMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
