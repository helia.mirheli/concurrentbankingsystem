package com.example.concurrentbankingsystem.userInterface;

import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountIn;
import com.example.concurrentbankingsystem.bl.Bank.model.BankAccountOut;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.BankService;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.command.DepositCommand;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.command.TransferCommand;
import com.example.concurrentbankingsystem.bl.Bank.repository.service.command.WithdrawCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;
import java.util.concurrent.ExecutionException;

@Component
public class UserConsoleInterface implements CommandLineRunner {
    private final BankService bankService;

    @Autowired
    public UserConsoleInterface(BankService bankService) {
        this.bankService = bankService;
    }

    @Override
    public void run(String... args) throws ExecutionException, InterruptedException {
        Scanner scanner = new Scanner(System.in);

        boolean exit = false;
        while (!exit) {
            System.out.println("Welcome to the Banking System!");
            System.out.println("1. Create Account");
            System.out.println("2. Deposit");
            System.out.println("3. Withdraw");
            System.out.println("4. transfer");
            System.out.println("5. Check Balance");
            System.out.println("0. Exit");

            System.out.print("Please enter your choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1 -> createAccount(scanner);
                case 2 -> performDeposit(scanner);
                case 3 -> performWithdraw(scanner);
                case 4 -> performTransfer(scanner);
                case 5 -> checkBalance(scanner);
                case 0 -> exit = true;
                default -> System.out.println("Invalid choice. Please try again.");
            }
            System.out.println();
        }
    }

    private void createAccount(Scanner scanner) {
        System.out.print("Enter account number: ");
        String accountNumber = scanner.nextLine();
        System.out.print("Enter account name: ");
        String accountName = scanner.nextLine();
        System.out.print("Enter initial balance: ");
        double balance = scanner.nextDouble();
        scanner.nextLine(); // Consume the newline character

        BankAccountIn accountIn = new BankAccountIn();
        accountIn.setAccountNumber(accountNumber);
        accountIn.setAccountName(accountName);
        accountIn.setBalance(balance);

        System.out.println("Account created successfully.");
        System.out.println("Account details: " + bankService.createAccount(accountIn));
    }

    private void performDeposit(Scanner scanner) throws ExecutionException, InterruptedException {
        System.out.print("Enter account number: ");
        String accountNumber = scanner.nextLine();
        System.out.print("Enter deposit amount: ");
        double amount = scanner.nextDouble();
        scanner.nextLine();

        System.out.println("Deposit was done successfully.");
        BankAccountOut account = bankService.performTransaction(new DepositCommand(accountNumber, amount)).get();
        System.out.println("Account details: " + account);
    }

    private void performWithdraw(Scanner scanner) throws ExecutionException, InterruptedException {
        System.out.print("Enter account number: ");
        String accountNumber = scanner.nextLine();
        System.out.print("Enter withdrawal amount: ");
        double amount = scanner.nextDouble();
        scanner.nextLine();

        System.out.println("Withdrawal was done successfully.");
        BankAccountOut account = bankService.performTransaction(new WithdrawCommand(accountNumber, amount)).get();
        System.out.println("Account details: " + account);
    }

    private void performTransfer(Scanner scanner) throws ExecutionException, InterruptedException {
        System.out.print("Enter sender account number: ");
        String fromAccountNumber = scanner.nextLine();
        System.out.print("Enter recipient account number: ");
        String toAccountNumber = scanner.nextLine();
        System.out.print("Enter transfer amount: ");
        double amount = scanner.nextDouble();
        scanner.nextLine();

        System.out.println("Transfer was done successfully.");
        BankAccountOut senderAccount =
                bankService.performTransaction(new TransferCommand(fromAccountNumber, toAccountNumber, amount)).get();
        System.out.println("Account details (sender): " + senderAccount);
    }

    private void checkBalance(Scanner scanner) {
        System.out.print("Enter account number: ");
        String accountNumber = scanner.nextLine();

        System.out.println("Account balance: " + bankService.getBalance(accountNumber));
    }
}
